import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import App from './App.vue'

// components
import Dashboard from './components/Dashboard.vue'
import Settings from './components/Settings.vue'

import 'vuetify/dist/vuetify.min.css'

Vue.use(VueRouter)
Vue.use(Vuetify)

const routes =  [
  { path: '/', component: App },
  { path: '/dashboard', component: Dashboard },
  { path: '/settings', component: Settings }
]

const router = new VueRouter({
  routes: routes,
  mode: 'history'
})

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  router
  //render: h => h(App)
}).$mount('#app')
